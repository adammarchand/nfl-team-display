
<?php
  /*
   Plugin Name: NFL Data Display
   Author: Adam Marchand
   Description: 
  */

  defined('ABSPATH') or die('Thou Shall Not Pass');

  function outputTeams() {
    $url = 'http://delivery.chalk247.com/team_list/NFL.JSON?api_key=74db8efa2a6db279393b433d97c2bc843f8e32b0';
    $response = file_get_contents($url);
    if ($response) {
      $decodedData = json_decode($response, true);
    }

    // The actual "good stuff" is a few items deep in the API result
    $teams = $decodedData['results']['data']['team'];

    // Define empty arrays that we can push results into by division
    $NFCNorth = $NFCSouth = $NFCEast = $NFCWest = [];
    $AFCNorth = $AFCSouth = $AFCEast = $AFCWest = [];

    // Now we loop through each team and we can easily access their properties (conference, division)
    foreach ($teams as $team) {

      if ($team['conference'] == 'NFC') {
        if ($team['division'] == 'North') {
          $NFCNorth[] = $team['name'];
        } elseif ($team['division'] == 'South') {
          $NFCSouth[] = $team['name'];
        } elseif ($team['division'] == 'East') {
          $NFCEast[] = $team['name'];
        } elseif ($team['division'] == 'West') {
          $NFCWest[] = $team['name'];
        }
      } elseif ($team['conference'] == 'AFC') {
        if ($team['division'] == 'North') {
          $AFCNorth[] = $team['name'];
        } elseif ($team['division'] == 'South') {
          $AFCSouth[] = $team['name'];
        } elseif ($team['division'] == 'East') {
          $AFCEast[] = $team['name'];
        } elseif ($team['division'] == 'West') {
          $AFCWest[] = $team['name'];
        }
      }

    } // End foreach($teams as $team)

    // Output teams
    // NFC
    echo '<h1>NFC</h1>';
    echo '<h2>North</h2>';
    echo '<ul>';
    foreach ($NFCNorth as $teamName) {
      echo '<li>' . $teamName . '</li>';
    }
    echo '</ul>';

    echo '<h2>South</h2>';
    echo '<ul>';
    foreach ($NFCSouth as $teamName) {
      echo '<li>' . $teamName . '</li>';
    }
    echo '</ul>';

    echo '<h2>East</h2>';
    echo '<ul>';
    foreach ($NFCEast as $teamName) {
      echo '<li>' . $teamName . '</li>';
    }
    echo '</ul>';

    echo '<h2>West</h2>';
    echo '<ul>';
    foreach ($NFCWest as $teamName) {
      echo '<li>' . $teamName . '</li>';
    }
    echo '</ul>';


    // AFC
    echo '<h1>AFC</h1>';
    echo '<h2>North</h2>';
    echo '<ul>';
    foreach ($AFCNorth as $teamName) {
      echo '<li>' . $teamName . '</li>';
    }
    echo '</ul>';

    echo '<h2>South</h2>';
    echo '<ul>';
    foreach ($AFCSouth as $teamName) {
      echo '<li>' . $teamName . '</li>';
    }
    echo '</ul>';

    echo '<h2>East</h2>';
    echo '<ul>';
    foreach ($AFCEast as $teamName) {
      echo '<li>' . $teamName . '</li>';
    }
    echo '</ul>';

    echo '<h2>West</h2>';
    echo '<ul>';
    foreach ($AFCWest as $teamName) {
      echo '<li>' . $teamName . '</li>';
    }
    echo '</ul>';

    return;
  }

  add_shortcode('nfltest', outputTeams);
?>
